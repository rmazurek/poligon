# Deploy

## Instalacja docker

Instrukcja instalacji docker: https://docs.docker.com/install/  

Instrukcja instalacji docker-compose: https://docs.docker.com/compose/install/#install-compose  

## Instalacja
- cp .env.dist .env
- mkdir var
- chmod 777 -R var
- cp docker-compose.yml.dev docker-compose.yml
- cp docker/web/vhost.conf.dev docker/web/vhost.conf
- docker-compose up
- CTRL+c
- docker-compose run web bash
- composer install
- bin/console doctrine:migrations:migrate
- bin/console cache:clear
- exit

Czasami po pierwszym uruchomieniu docker-compose up wyrzuca błędy. Wtedy po zakmnięciu CTRL+c należy ponownie uruchomić docker-compose up i ponownie zamknąć.  

Po zainstalowaniu uruchomić dockera: docker-compose up
Domyślnie apache dostępny jest na porcie 8080 a baza 33306

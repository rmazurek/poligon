<?php // src/Entity/Ip

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * IP Entity.
 * 
 * @ORM\Entity(repositoryClass="App\Repository\IpRepository")
 * @UniqueEntity("address")
 */
class Ip
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;
    
    /**
     * IP address
     * 
     * @var IP address
     * 
     * @ORM\Column("string")
     * @Assert\Ip
     * @Assert\NotBlank
     */
    protected $address;

    /**
     * Get ID.
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set IP address
     * 
     * @param string $address
     * 
     * @return void
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }
    
    /**
     * Get IP address.
     * 
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }
}

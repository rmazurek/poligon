<?php // src/Controller/Nip.php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use GusApi\Exception\InvalidUserKeyException;
use GusApi\GusApi;
use GusApi\ReportTypes;

/**
 * GUS API test page.
 */
class Nip extends AbstractController
{
    /**
     * @Route("/nip")
     */
    public function index()
    {
        $gus = new GusApi('abcde12345abcde12345', 'dev');
        $nip = '5242289484';
        try {
            $nipToCheck = $nip; //change to valid nip value
            $gus->login();
            $gusReports = $gus->getByNip($nipToCheck);
            $name = $gusReports[0]->getName();
            $regon = $gusReports[0]->getRegon();
            $province = $gusReports[0]->getProvince();
            $district = $gusReports[0]->getDistrict();
            $community = $gusReports[0]->getCommunity();
            $place = $gusReports[0]->getCity();
            $zipcode = $gusReports[0]->getZipCode();
            $street = $gusReports[0]->getStreet();
        } catch (InvalidUserKeyException $e) {
            echo 'Bad user key';
        } catch (\GusApi\Exception\NotFoundException $e) {
            echo 'No data found <br>';
            echo 'For more information read server message below: <br>';
            echo $gus->getResultSearchMessage();
        }
        return $this->render('nip/index.html.twig',[
            'nip' => $nip,
            'name' => $name,
            'regon' => $regon,
            'province' => $province,
            'district' => $district,
            'community' => $community,
            'place' => $place,
            'zipcode' => $zipcode,
            'street' => $street,
        ]);
    }
}

<?php // src/Controller/Ip.php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\IpType;
use App\Entity\Ip as IpEntity;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Ip controller.
 */
class Ip extends AbstractController
{
    /**
     * IP information page.
     * 
     * Display client IP address.
     * If address not in database, then display 403 Forbidden page.
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     * 
     * @Route("/ip")
     */
    public function index(Request $request)
    {
        $ip = $request->getClientIp();
        if(!$this->getDoctrine()->getRepository(IpEntity::class)->check($ip)){
            throw new AccessDeniedHttpException("Address $ip not allowed");
        }
        return $this->render('ip/index.html.twig',[
            'ip' => $ip,
        ]);
    }
    
    /**
     * IP address form.
     * 
     * Enables to provide IP address to store in database.
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     * 
     * @Route("/ip/form")
     */
    public function form(Request $request)
    {
        $form = $this->createForm(IpType::class, new IpEntity());
        $form->handleRequest($request);
        if($form->isSubmitted() and $form->isValid()){
            $this->getDoctrine()->getRepository(IpEntity::class)->save($form->getData());
            $this->addFlash('success', 'IP saved');
        }
        
        return $this->render('ip/form.html.twig',[
            'form' => $form->createView(),
        ]);
    }
    
    
}

<?php // src/Repository/IpRepository.php

namespace App\Repository;

use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use App\Entity\Ip as IpEntity;

/**
 * IP entity repository.
 */
class IpRepository extends ServiceEntityRepository
{
    /**
     * Class constructor.
     * 
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, IpEntity::class);
    }
    
    /**
     * Save ip entity.
     * 
     * @param IpEntity $ip
     * 
     * @return void
     */
    public function save(IpEntity $ip)
    {
        $this->_em->persist($ip);
        $this->_em->flush();
    }
    
    /**
     * Check if ip address exists in database.
     * If address is in database, returns true.
     * Otherwise returns false.
     * 
     * @param string $ip
     * 
     * @return boolean
     */
    public function check(string $ip)
    {
        $addresses = $this->findAll();
        foreach($addresses as $address){
            if($ip == $address->getAddress()){
                return true;
            }
        }
        return false;
    }
}
